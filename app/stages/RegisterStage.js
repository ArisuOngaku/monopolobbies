import Stage from './Stage.js'

import HomeStage from './HomeStage.js'

import { set_bad_request_response } from '../utils/http-helper.js'

/** @typedef {import('../Client.js').default} Client */

export default class RegisterStage extends Stage {
    
    listeners = {
        socket: [],
        request: [(requestBody, res) => this.request_listener(requestBody, res)]
    };

    /**
     * @param {Client} client
     */
    constructor(client) {
        super('register', '/', client);
    }

    request_listener(requestBody, res) {
        if (requestBody.type !== this.get_event_name()) {
            return false;
        }

        if (typeof requestBody.username !== 'string' || requestBody.username.length < 3 || requestBody.username.length > 30) {
            set_bad_request_response(res);
            return true;
        }

        this.client.username = requestBody.username;
        
        // @ts-ignore
        this.client.set_stage(new HomeStage(this.client));
        res.redirect('/home');
        res.end();
        return true;
    }
}