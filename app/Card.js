import fs from 'fs'

import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { TurnState, TaxType } from './Turn.js';
import { PlayerStatus } from './Player.js';


/** @typedef {import('./Player.js').default} Player */

// @ts-ignore
const DIRNAME = dirname(fileURLToPath(import.meta.url));



/**
 * @param {(player: Player) => number | number} tax_amount
 * @returns {(player: Player) => number}
 */
function gen_action_tax(tax_amount) 
{
    return  /** @param {Player} player */ (player) => {
        let tax_amt = (typeof tax_amount === "function") ? tax_amount(player) : tax_amount;
        player.game.transfer_money(player, undefined, tax_amt);
        return tax_amt;
    };
}

/**
 * @param {number} location
 * @returns {(player: Player) => number}
 */
function gen_action_advance(location)
{
    return /** @param {Player} player */ (player) => {
        player.game.advance_player_to(player, location);
        return location;
    }
}

/**
 * @param {string} target
 * @returns {(player: Player) => void}
 */
function gen_action_near_advance(target)
{
    return /** @param {Player} player */ (player) => {
        if(target === 'railroad') {
            let railroads = player.game.board.get_railroads().sort((r1, r2) => {
                let r1_dist = r1.location - player.position;
                let r2_dist = r2.location - player.position;
                
                if(r1_dist < 0) {
                    r1_dist += 40;
                }
                if(r2_dist < 0) {
                    r2_dist += 40;
                }

                return r1_dist - r2_dist;
            });
            let pay_tax = (!!railroads[0].owner && railroads[0].owner !== player);
            player.game.teleport(player, railroads[0].location, pay_tax);
            if(pay_tax) {
                let cost = railroads[0].get_rent(player.game) * 2;
                player.game.current_turn.cur_tax = {type: TaxType.RENT, dest_id:railroads[0].owner.get_id(), amount: cost};
                player.game.current_turn.step = TurnState.TAX;
                if(player.money < cost) {
                    player.game.current_turn.resolve_bankrupcy();
                }
                else {
                    player.game.notify_turn(false);
                }
            }
        }
        else if(target === 'utility') {
            let utilities = player.game.board.get_utilities().sort((r1, r2) => {
                let r1_dist = r1.location - player.position;
                let r2_dist = r2.location - player.position;
                
                if(r1_dist < 0) {
                    r1_dist += 40;
                }
                if(r2_dist < 0) {
                    r2_dist += 40;
                }

                return r1_dist - r2_dist;
            });

            let pay_tax = (!!utilities[0].owner && utilities[0].owner !== player);
            player.game.teleport(player, utilities[0].location, pay_tax);
            if(pay_tax) {
                let cost = 10 * (Math.floor(Math.random() * 6 + 1) + Math.floor(Math.random() * 6 + 1));
                player.game.current_turn.cur_tax = {type: TaxType.RENT, dest_id:utilities[0].owner.get_id(), amount: cost};
                player.game.current_turn.step = TurnState.TAX;
                if(player.money < cost) {
                    player.game.current_turn.resolve_bankrupcy();
                }
                else {
                    player.game.notify_turn(false);
                }
            }
        }
    }
}

/**
 * @returns {(player: Player) => void}
 */
function gen_action_jail() {
    return /** @param {Player} player */ (player) => {
        player.game.jail(player);
    }
}

/**
 * 
 * @param {number} amount 
 * @returns {(player: Player) => void}
 */
function gen_action_receive(amount) {
    return /** @param {Player} player */ (player) => {
        player.game.transfer_money(undefined, player, amount)
    }
}

/**
 * 
 * @param {number} amount 
 * @returns {(player: Player) => void}
 */
function gen_action_steal(amount) {
    return /** @param {Player} player */ (player) => {
        for(const stealer of player.game.players) {
            if(stealer === player || stealer.status !== PlayerStatus.ACTIVE) {
                continue;
            }
            player.game.transfer_money(stealer, player, amount);
        }
    }
}

/**
 * 
 * @returns {(player: Player) => void}
 */
function gen_action_jail_out() {
    return /** @param {Player} player */ (player) => {
        player.get_out_of_jail = true;
    }
}

/**
 * @param {number} amount
 * @returns {(player: Player) => void}
 */
function gen_action_back(amount)
{
    return /** @param {Player} player */ (player) => {
        let target = player.position - amount;
        if(target < 0) {
            target += 40;
        }
        player.game.teleport(player, target);
    }
}

/**
 * @param {number} house
 * @param {number} hotel
 * @returns {(player: Player) => void}
 */
function gen_action_repair(house, hotel)
{
    return /** @param {Player} player */ (player) => {
        let cost = 0;
        let owned = player.game.board.get_properties_of(player);
        for(const property of owned) {
            if(property.house_count === 5) {
                cost += hotel;
            }
            else {
                cost += property.house_count * house;
            }
        }
        player.game.current_turn.cur_tax = {type: TaxType.SIMPLE, dest_id:-1, amount: cost};
        player.game.current_turn.step = TurnState.TAX;
        if(player.money < cost) {
            player.game.current_turn.resolve_bankrupcy();
        }
        else {
            player.game.notify_turn(false);
        }
    }
}

const card_actions = {
    tax: gen_action_tax,
    receive: gen_action_receive,
    advance: gen_action_advance,
    jail: gen_action_jail,
    near_advance: gen_action_near_advance,
    steal: gen_action_steal,
    jail_out: gen_action_jail_out,
    back: gen_action_back,
    repair: gen_action_repair
};

let cards = {};

/**
 * Loads card into cache from fs
 * @param {string} deckname 
 * @param {string} name 
 */
function generate_card(deckname, name) {
    let cardData;
    {
        try {
            let cardfd = fs.openSync(DIRNAME + "/decks/" + deckname + "/" + name + ".json", 'r');
            if(cardfd <= -1)
            {
                console.log("Error while generating card "+name+": /decks/"+deckname+"/"+name+".json not opened.");
                return undefined;
            }

            let cardContent = fs.readFileSync(cardfd);
            fs.closeSync(cardfd);
            cardData = JSON.parse(cardContent.toString());
        }
        catch(err) {
            console.log('Error while generating card '+name+': '+err);
            return undefined;
        }
    }
    let card = new Card();
    card.description = cardData.description;
    if(cardData.action) {
        if(cardData.action.type === 'tax') {
            card.action = card_actions.tax(cardData.action.amount);
        }
        else if(cardData.action.type === 'advance') {
            card.action = card_actions.advance(cardData.action.location);
        }
        else if(cardData.action.type === 'jail') {
            card.action = card_actions.jail();
        }
        else if(cardData.action.type === 'receive') {
            card.action = card_actions.receive(cardData.action.amount);
        }
        else if(cardData.action.type === 'near_advance') {
            card.action = card_actions.near_advance(cardData.action.target);
        }
        else if(cardData.action.type === 'steal') {
            card.action = card_actions.steal(cardData.action.amount);
        }
        else if(cardData.action.type === 'jail_out') {
            card.action = card_actions.jail_out();
        }
        else if(cardData.action.type === 'back') {
            card.action = card_actions.back(cardData.action.amount);
        }
        else if(cardData.action.type === 'repair') {
            card.action = card_actions.repair(cardData.action.value_house, cardData.action.value_hotel);
        }
    }
    if(!cards[deckname]) {
        cards[deckname] = {};
    }
    cards[deckname][name] = card;
    return card;
}

/**
 * Fetches card from cache
 * @param {string} deckname Name of the deck to look up card in
 * @param {string} name Name of the card to fetch
 */
export function get_card(deckname, name) {
    if(!cards[deckname] || !cards[deckname][name]) {
        generate_card(deckname, name);
    }
    if(!cards[deckname])
        return undefined;
    return cards[deckname][name];
}

export default class Card {

    description = "";

    /** @type {(player: Player) => *} */
    action = (player) => {};

    constructor() {

    }

    copy() {

    }


}