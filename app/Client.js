import Stages from './Stages.js';

/** @typedef {import('./Server.js').default} Server */
/** @typedef {import('socket.io').Socket} SocketClient */
/** @typedef {import('./stages/Stage.js').default} Stage */
/** @typedef {import('./stages/ChatStage').default} ChatStage */

export default class Client {

    /** @type {Server} */
    server;
    /** @type {SocketClient[]} */
    sockets = [];
    /** @type {Number} */
    id;

    session = undefined;
    /** @type {String} */
    username = undefined;

    /** @type {ChatStage} */
    chatHandler = undefined;


    /**
     * @private
     * @type {((requestBody:any, response:any) => boolean)[]}
     */
    activeReqListeners = [];

    /**
     * @private
     */
    activeSocketSubscribers = {};


    /** @private */
    timeout = 2;
    /** @private @type {NodeJS.Timeout} */
    timeoutVal = undefined;

    /**
     * @param {!Server} server 
     * @param {any} session 
     * @param {Number} id 
     */
    constructor(server, session, id) {
        this.server = server;
        this.session = session;
        this.id = id;
        this.timeoutVal = setInterval(() => this.check_timeout(), 30*1000);

        // @ts-ignore
        this.set_stage(new Stages.REGISTER(this));
    }

    check_timeout() {
        if(this.sockets.length == 0) {
            this.timeout--;
            if(this.timeout < 0) {
                clearInterval(this.timeoutVal);
                this.current_stage.leave();
                this.server.disconnect_client(this);
            }
        }
        else {
            this.timeout = 2;
        }
    }

    /**
     * Emits event to client socket.
     * @param {String} event 
     * @param {Object} msg
     */
    emit(event, msg) {
        this.sockets.forEach((socket) => socket.emit(event, msg));
    }

    /**
     * Subscribe stage event listeners
     * @param {!Stage} stage
     */
    subscribe(stage) {
        let subscriber = (socket) => {
            stage.listeners.socket.forEach(listener => {
                socket.on(stage.get_event_name(), (data) => listener(data, socket));
            });
        };

        this.activeSocketSubscribers[stage.get_event_name()] = subscriber;

        this.sockets.forEach((socket) => subscriber(socket));
        

        stage.listeners.request.forEach(listener => {
            this.activeReqListeners.push(listener);
        });
    }

    /**
     * Unsubscribe stage event listeners
     * @param {!Stage} stage
     */
    unsubscribe(stage) {
        delete(this.activeSocketSubscribers[stage.get_event_name()]);

        stage.listeners.socket.forEach(listener => {
            this.sockets.forEach(socket => {
                socket.off(stage.get_event_name(), listener);
            });
        });

        stage.listeners.request.forEach(listener => {
            this.activeReqListeners = 
                this.activeReqListeners.filter((val) => val !== listener);
        });
    }

    /**
     * @param {string} url
     * @param {any} res
     */
    check_url_and_redirect(url, res) {
        const target_url = this.current_stage.get_home_url();
        if(url !== target_url) {
            res.redirect(target_url);
            res.end();
            return true;
        }
        return false;
    }

    /**
     * 
     * @param {any} requestBody 
     * @param {any} res
     */
    handle_request(requestBody, res) {
        if (!requestBody.type) {
            return false;
        }

        for (const listener of this.activeReqListeners) {
            if (listener(requestBody, res)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param {!Stage} stage
     */
    set_stage (stage) {
        // Leave current stage
        if (this.current_stage) {
            this.current_stage.leave();
            this.unsubscribe(this.current_stage);
        }

        // Enter new stage
        this.current_stage = stage;
        this.subscribe(stage);
        this.current_stage.enter();
    }

    /**
     * Adds Socket to Client
     * @param {!SocketClient} socket 
     */
    connect_socket(socket) {
        this.sockets.push(socket);
        for(const eventName in this.activeSocketSubscribers) {
            this.activeSocketSubscribers[eventName](socket);
        }
    }

    /**
     * Removes Socket from Client
     * @param {!SocketClient} socket 
     */
    disconnect_socket(socket) {
        this.sockets = this.sockets.filter((otherSocket) => otherSocket !== socket);
    }

}