/** @typedef {{author:string, message:string}} ChatMessage */
/** @typedef {import('./stages/ChatStage').default} ChatStage */

const CHANNEL_HISTORY_MAX_SIZE = 100;

/** @interface */
export default class ChatChannelHolder {

    /**
     * @abstract
     * @param {!string} name
     * @returns {?ChatChannel}
     */
    get_channel(name) { return undefined; }
}

export class ChatChannel {

    /** 
     * @type {string}
     */
    name;

    /**
     * @type {string}
     */
    display_name;

    /**
     * @private @type {ChatMessage[]}
     */
    history = [];

    max_history = CHANNEL_HISTORY_MAX_SIZE;

    /**
     * @private @type {ChatStage[]}
     */
    listeners = [];

    /**
     * @param {string} name
     * @param {string} display_name 
     */
    constructor(name, display_name) {
        this.name = name;
        this.display_name = display_name;
    }

    /**
     * @returns {ChatMessage[]}
     */
    get_history() {
        return this.history;
    }

    /**
     * @param {ChatMessage} message 
     */
    add_message(message) {
        this.history.push(message);
        if(this.history.length > this.max_history) {
            this.history = this.history.slice(1);
        }

        this.listeners.forEach(listener => listener.on_chat_message(this.name, message));
    }

    /**
     * @param {ChatStage} listener
     */
    add_listener(listener) {
        this.listeners.push(listener);
    }

    /**
     * @param {ChatStage} listener
     */
    remove_listener(listener) {
        this.listeners = this.listeners.filter(l => l !== listener);
    }
}